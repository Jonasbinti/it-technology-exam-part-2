# This code is used on the coordinator Xbee module, this program is responsible for sending the turn on/off instructions
# Note that for both the programs to work together,
# they should both be connected via two usb adapters to a windows machine

from digi.xbee.devices import XBeeDevice
from time import sleep


def setup():
    device = XBeeDevice('COM3', 9600)
    ep_device_id = "master_xbee"
    device.open()
    xbee_network = device.get_network()
    ep_device = xbee_network.discover_device(ep_device_id)
    return device, ep_device
    if ep_device is None:
        print("Could not find the remote device")
        exit(1)

def data_received(xbee_message):
    data = str((xbee_message.data.decode()))
    print('\n', 'Response from', ':', data)

xbee_device, ep_device = setup()
xbee_device.add_data_received_callback(data_received)

try:
    while True:
        input_to_send = str(input("Input a message:"))
        print("Sending message to", ep_device.get_64bit_addr(), ":", input_to_send)
        xbee_device.send_data(ep_device, input_to_send)
        sleep(1)
finally:
    xbee_device.close()
