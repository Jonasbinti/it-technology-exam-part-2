import serial

try:
    node = serial.Serial("/dev/ttyUSB0", 9600)
    node.open()
    print("serial open")
    node.read()
except serial.SerialException:
    node.close()