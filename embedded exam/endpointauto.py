from digi.xbee.devices import XBeeDevice



device = XBeeDevice("/dev/ttyUSB0", 9600)
external_xbee = "master_xbee"


try:
    device.open()
except: 
    print('device could not open')
    exit(0)

xbee_network = device.get_network()
control_device = xbee_network.discover_device(external_xbee)

def data_received(xbee_message):
    message = str(xbee_message.data.decode())
    print("message recived from", control_device,":", message)
    print("Sending message to", control_device,": message recived")
    device.send_data(control_device, "message recived" )


device.add_data_received_callback(data_received)
input("awaiting for message")

    
