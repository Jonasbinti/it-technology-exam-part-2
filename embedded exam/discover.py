
from digi.xbee.models.status import NetworkDiscoveryStatus
from digi.xbee.devices import XBeeDevice
from time import sleep
from digi.xbee.models.options import DiscoveryOptions


def main():
    cord_xbee = XBeeDevice('COM3', 9600)

    try:
        cord_xbee.open()
        xbee_network = cord_xbee.get_network()
        xbee_network.set_discovery_timeout(10)
        xbee_network.set_discovery_options({DiscoveryOptions.DISCOVER_MYSELF})
        xbee_network.clear()

        def device_discovered(remote):
            print("Device found on network:", str(remote))

        def discovery_done(discovery_status):
            if discovery_status == NetworkDiscoveryStatus.SUCCESS:
                print("Network discovery finished")
            else:
                print(str(discovery_status.description))

        xbee_network.add_device_discovered_callback(device_discovered)

        xbee_network.add_discovery_process_finished_callback(discovery_done)

        xbee_network.start_discovery_process()

        print("Looking for devices on network")
        while xbee_network.is_discovery_running():
            sleep(0.1)
    finally:
        if cord_xbee is not None and cord_xbee.is_open():
            cord_xbee.close()


if __name__ == '__main__':
    main()
