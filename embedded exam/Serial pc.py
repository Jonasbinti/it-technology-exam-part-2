import serial

try:
    node = serial.Serial("COM4", 9600)
    node.open()
    node.write("Open serial")
except serial.SerialException:
    node.close()
