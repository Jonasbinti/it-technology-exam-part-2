import serial

try:
    node = serial.Serial("COM4", 9600)
    node.open()
    print("Serial open")
    node.write("Hello world")
except serial.SerialException:
    node.close()
