import serial

try:
    node = serial.Serial("/dev/ttyUSB0", 9600)
    node.open()
    node.write('Open serial')
except serial.SerialException:
    node.close()
